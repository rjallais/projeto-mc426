// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {


	  // apiKey: "AIzaSyChad-Bct7xBtcnXOROnkteeV4cFbf_iRU",
    // authDomain: "projeto-mc426-2547e.firebaseapp.com",
    // projectId: "projeto-mc426-2547e",
    // storageBucket: "projeto-mc426-2547e.appspot.com",
    // messagingSenderId: "1019825442580",
    // appId: "1:1019825442580:web:21df1942941c95be68bc01",
    // measurementId: "G-ZPYZ00XDEY"


	//Outro servidor para testes caso o oficial fique sem acessos gratuitos

  // apiKey: "AIzaSyDbPiQKdQIgV9qmuETA8Es6G4ShRmXOaMs",
  // authDomain: "serifil.firebaseapp.com",
  // projectId: "serifil",
  // storageBucket: "serifil.appspot.com",
  // messagingSenderId: "833289921432",
  // appId: "1:833289921432:web:03e8ae57fd52dff2a041ed",
  // measurementId: "G-G2NS97V2LQ"


  apiKey: "AIzaSyBv3tCuo4f4Z6oNxbyPF3kcB6bmNTPtfcU",
  authDomain: "mc426buscador.firebaseapp.com",
  projectId: "mc426buscador",
  storageBucket: "mc426buscador.appspot.com",
  messagingSenderId: "706984808907",
  appId: "1:706984808907:web:a6458e583fd543fb50a64e",
  measurementId: "G-5G6W2RYQDW"
 
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
