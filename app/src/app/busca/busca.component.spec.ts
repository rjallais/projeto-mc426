import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DataService } from '../data.service';
import { ResultadoComponent } from '../resultado/resultado.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../environments/environment';

import { BuscaComponent } from './busca.component';

describe('BuscaComponent', () => {
  let component: BuscaComponent;
  let fixture: ComponentFixture<BuscaComponent>;

  let resultado: ResultadoComponent;
  let fixtureR: ComponentFixture<ResultadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscaComponent, ResultadoComponent ],
      imports: [
        AngularFireModule.initializeApp(environment.firebaseConfig),
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    fixtureR = TestBed.createComponent(ResultadoComponent);
    resultado = fixtureR.componentInstance;
    fixtureR.detectChanges();


  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Teste 2 should change busca', () => {
    let data : DataService;
    data = new DataService()

    component.nome = "The Matrix";
    component.newMessage();

    expect(resultado.getNome()).toEqual("The Matrix");
  });

  it('Teste 2 should not change busca', () => {
    let data : DataService;
    data = new DataService()

    component.nome = "The Matrix";
    component.newMessage();
    component.newMessage();
    
    expect(resultado.getNome()).toEqual("The Matrix");
  });
});
