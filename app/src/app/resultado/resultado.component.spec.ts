import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed,tick,fakeAsync } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { By } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
import { BuscaComponent } from '../busca/busca.component';

import { ResultadoComponent } from './resultado.component';

describe('ResultadoComponent', () => {
  let component: ResultadoComponent;
  let fixture: ComponentFixture<ResultadoComponent>;
  let busca : BuscaComponent
  let fixturebusca: ComponentFixture<BuscaComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadoComponent ],
      imports: [AngularFireModule.initializeApp(environment.firebaseConfig)]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    fixturebusca = TestBed.createComponent(BuscaComponent);
    busca = fixturebusca.componentInstance;
    fixturebusca.detectChanges();
  });

  it('Teste 1 - teste de mensagem de erro para um filme não existente', () => {
    busca.ehfilme = true;
    busca.nome = "Filme não existente";
    busca.newMessage();

    expect(component.getEstado()).toBeFalse();
  });

  
  it('Teste 1 - teste de mensagem de erro para um filme existente', () => {
    busca.nome = "The matrix";
    busca.newMessage();
    busca.ehfilme = true;
    const bannerDe: DebugElement = fixture.debugElement;
    const paragraphDe = bannerDe.query(By.css('.erro'));
    expect(paragraphDe).toBeFalsy();
  });

  it('Teste 1 - teste de mensagem de erro para uma leitura vazia', () => {
    busca.nome = "";
    busca.newMessage();
    busca.ehfilme = true;
    const bannerDe: DebugElement = fixture.debugElement;
    const paragraphDe = bannerDe.query(By.css('.erro'));
    expect(paragraphDe).toBeFalsy();
  });

  it('Teste 3 - Exibe o filme válido', () => {
    busca.ehfilme = true;
    busca.nome = "The Matrix";
    busca.newMessage();
    expect(component.getNomeDOC()).toContain(busca.getNome().toUpperCase());
  });

  it('Teste 3 - Exibe a série válida', () => {
    busca.ehfilme = false;
    busca.nome = "Anne with an E";
    busca.newMessage();
    expect(component.getNomeDOC()).toContain(busca.getNome().toUpperCase());
  });

  it('Teste 3 - Não exibe nem série nem filme', () => {
    busca.nome = "";
    busca.newMessage();
    if (busca.ehfilme) {
      expect(component.getNomeDOC()).toEqual('movies/nada');
    }
    else{
      expect(component.getNomeDOC()).toEqual('series/nada');
    }
    
  });


});
