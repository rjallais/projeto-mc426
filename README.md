# Projeto MC426

Projeto da disciplina de Engenharia de Software - MC426 - da graduação UNICAMP 2s2020

O projeto consiste de uma ferramenta capaz de encontrar as plataformas de streaming que disponibilizam o filme ou série que deseja assistir.

# Padrões de Projeto

### Diagrama em nível de componentes

![image](/images/uml_mc426.png)

### Estilo Arquitetural

Como nosso projeto se trata de uma aplicação web decidimos utilizar o Angular 8, por ser um framework amplamente utilizado para este fim, o estilo de arquitetura do angular é o Model-View-Controller (MVC). Como o MVC permite a separação entre a apresentação da interface gráfica para o usuário e as funcionalidades da aplicação e facilmente integrar as duas. Este estilo possibilitou ao grupo desenvolver separadamente cada segmento do projeto, permitindo que cada setor fosse concluído e testado separadamente.

### Principais Componentes

Os principais componentes presentes na aplicação são:

busca.component: O componente 'busca' é o Provedor da aplicação, ele recebe no atributo 'nome' o título da obra a ser procurado e emite a notificação através do método 'newMessage()' quando 'nome' for alterado.

data.service: O componente 'data' é o Action Handler da aplicação, assim, no momento que o 'busca' notifica alteração de estado, o 'data' é responsável por chamar o método 'update()' dos observadores através do método 'changeMessage()'.

filmes.component e series.component: Ambos são os observadores da aplicação, sendo assim, recebem a notifição e atualizam, através do método 'update()', qual o título do filme ou série a ser buscado . Além disso, são responsáveis por se conectar ao database Cloud Firestore e requisitar os dados da obra e quais plataformas estão disponíveis.

### Padrão de Projeto

O padrão de projeto implementado para essa atividade foi o Observer, essa decisão se deu porque observou-se que mais de um componente dependia de apenas 1, concluindo assim que este padrão seria adequado para o nosso projeto.

# Tutorial para executar a aplicação

### Dependências necesssárias

A máquina deve ter instalado o 'NodeJS' e seu gerenciador de pacotes 'npm'. É possível verificar se estão instalados através dos comandos:  
`node --version`  
`npm --version`  
  
Com esses módulos deve ser instalado o Angular e o Firebase através do comando:  
`sudo npm install -g @angular/cli firebase`
  
Para confirmar a instalação execute o seguinte comando:  
`ng --version`  
  
Verifique todos os packages do Angular, caso algum esteja `<error>` não funcionará. Uma possível solução é executar os comandos seguintes:  
`npm install`  
`ng update`  
`npm update`  

### Executando a aplicação

Após isso basta entrar na pasta 'app' e executar o comando:  
`ng serve`  
  
Aparecerá no terminal um link para a página web que deve ser aberta para vizualizar a aplicação. Segue um vídeo neste [link](https://www.youtube.com/watch?v=c8Ff5JKTmHw&feature=youtu.be) demonstrando algumas funcionalidades.
